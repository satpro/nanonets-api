from django.urls import path
from .views import *

urlpatterns = [
    path('', FileUploadView.as_view()),
    path('addmodel/', add_model),
    path('trainmodel/', train_model),
    path('testmodel/', test_model),
]