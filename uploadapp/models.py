# Create your models here.
from django.db import models

learning_rates = ((0.001, '0.001'), (0.01, '0.01'), (0.1, '0.1'))
number_of_layers = ((1, '1'), (2, '2'), (4, '4'))
number_of_steps = ((1000, '1000'), (2000, '2000'), (4000, '4000'))

class MLModel(models.Model):
    model_id = models.AutoField(primary_key = True)
    model_name = models.CharField(max_length = 30, unique = True)
    test_file = models.FileField(upload_to = 'testModel')
    train_file = models.FileField(upload_to = 'trainModel')

class TrainingImage(models.Model):
    training_id = models.AutoField(primary_key = True)
    ml_model = models.ForeignKey(MLModel, on_delete=models.CASCADE)
    image_files = models.FileField(upload_to = 'trainImages')

class BestResult(models.Model):
    result_id = models.AutoField(primary_key = True)
    ml_model = models.OneToOneField(MLModel, on_delete=models.CASCADE)
    rate = models.CharField(max_length = 7, choices = learning_rates)
    layers = models.CharField(max_length = 7, choices = number_of_layers)
    steps = models.CharField(max_length =  7, choices = number_of_steps)

class File(models.Model):
    file = models.FileField(blank=False, null=False)
    def __str__(self):
        return self.file.name