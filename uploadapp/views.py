from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

# Create your views here.
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from django.http import JsonResponse
import zipfile
import base64
import subprocess
import threading
import os
import random

from .serializers import FileSerializer
from .models import MLModel
from .models import TrainingImage
from .models import BestResult

learning_rates = ((0.001, '0.001'), (0.01, '0.01'), (0.1, '0.1'))
number_of_layers = ((1, '1'), (2, '2'), (4, '4'))
number_of_steps = ((1000, '1000'), (2000, '2000'), (4000, '4000'))

class FileUploadView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):

      file_serializer = FileSerializer(data=request.data)

      if file_serializer.is_valid():
          file_serializer.save()
          return Response(file_serializer.data, status=status.HTTP_201_CREATED)
      else:
          return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
@csrf_exempt
def add_model(request):

    # get ml_model details from request post data: test_file, train_file, model_name
    # save into db
    if(request.method == 'POST'):
        print(request.POST)
        ml_model = MLModel(model_name = request.POST['modelName'], test_file = request.FILES['testFile'], train_file = request.FILES['trainFile'])
        ml_model.save()
        return JsonResponse({'message': 'Model added'}, status=201)
    else:
        return JsonResponse({'error': 'BAD_REQUEST'}, status=400)
@csrf_exempt
def train_model(request):

    # get model_name, training_files
    # get ml_model id using model_name
    # save to training_image table
    # start new thread to train data (async), call train_data
    # return response
    # 
    if(request.method == 'POST'):
        zip_file = request.FILES['trainingZip']
        if not os.path.exists('trainingImages'):
            os.mkdir('trainingImages')
        with zipfile.ZipFile(zip_file, 'r') as z:
            z.extractall('trainingImages')
        trainingImage = TrainingImage(ml_model_id = request.POST['modelId'], image_files= zip_file)
        trainingImage.save()
        t1 = threading.Thread(target=train_data, args=(request.POST['modelId'], 'trainingImages'))
        t1.start()
        return JsonResponse({'MESSAGE': 'MODEL TRAINED'}, status=200)

@csrf_exempt
def test_model(request):

    # get image, model_name from request data
    # get ml_model from model_name
    # get best_result params using ml_model
    # run test_file with best params for image
    # return response
    if(request.method == 'POST'):
        testImg = request.FILES['testImage']
        bestResult = BestResult.objects.get(ml_model__model_id =request.POST['modelId'])
        ml_model = MLModel.objects.get(model_id = request.POST['modelId'])
        test_file = ml_model.test_file
        i = bestResult.rate
        j = bestResult.layers
        k = bestResult.steps
        pathdir = "testingFile/" + str(testImg)
        if not os.path.exists(pathdir):
            os.makedirs(pathdir)
        acc = subprocess.run(["python media/", str(test_file), "--i", i,"--j", j, "--k", k, "--images "+ str(pathdir)], shell=True, capture_output=True)
        print("Message from test.py :", acc)
        return JsonResponse({'MESSAGE': 'Image tested'}, status=200)

        


def train_data(modelId, training_images):

    # get images from training_images
    # get train_file from ml_model
    # run train_file for 27 combinations
    # store best_result in BestResult table
    best_i = 0.001
    best_j = 1
    best_k = 1000
    ml_model = MLModel.objects.get(model_id= modelId)
    train_file = ml_model.train_file
    acc = 0
    
    for i in learning_rates:
        for j in number_of_layers:
            for k in number_of_steps:
                lr = i[0]
                nl = j[0]
                ns = k[0]
                cmd = "python " + "media/"+str(train_file) + " --i " + str(lr) + " --j " + str(nl) + " --k " + str(ns) + " --images " + str(training_images)
                msg = subprocess.run(cmd, shell=True, capture_output=True, timeout=None)

                accuracy = random.random()
                if(acc < accuracy):
                    acc = accuracy
                    best_i = i[0]
                    best_j = j[0]
                    best_k = k[0]
    print("Message from train.py: ", msg)
    bestResult = BestResult(ml_model_id = modelId, rate = best_i, layers = best_j, steps = best_k)
    bestResult.save()
    os.remove(training_images)